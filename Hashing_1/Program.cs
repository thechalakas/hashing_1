﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Hashing_1
{
    class Program
    {
        static void Main(string[] args)
        {

            //hashing means being able to use a unique code to refer to each type of content

            //lets get two strings
            Console.WriteLine("Enter two strings, we will tell if they are same using hash");

            Console.WriteLine("enter the first sentence");
            string input_string_1 = Console.ReadLine();
            Console.WriteLine("enter the second sentence");
            string input_string_2 = Console.ReadLine();

            //getting the hashes

            //we need a bytestring converter
            UnicodeEncoding bytestring_converter = new UnicodeEncoding();

            //we need a hashing object
            SHA256 hashing_object = SHA256.Create();

            //now, lets turn those strings into bytes
            byte[] input_string1_byte = bytestring_converter.GetBytes(input_string_1);
            byte[] input_string2_byte = bytestring_converter.GetBytes(input_string_2);

            //finally, we can hash it (man, there are so many steps!)
            byte[] input_string1_hash = hashing_object.ComputeHash(input_string1_byte);
            byte[] input_string2_hash = hashing_object.ComputeHash(input_string2_byte);

            //now, lets compare the two
            if(input_string1_hash.SequenceEqual(input_string2_hash))
            {
                Console.WriteLine("you have entered the same sentence twice");
            }
            else
            {
                Console.WriteLine("you have NOT entered the same sentence twice");
            }


            //now, lets do some code-blocking to prevent the console from dissapearing.
            Console.ReadLine();
        }
    }
}
